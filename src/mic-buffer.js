var MicBuffer = (function () {
  'use strict';

  var MicBuffer = function () {
    let validBufferSizes = [0, 256, 512, 1024, 2048, 4096, 8192, 16384];
    let statusStrings = ['Error', 'Microphone Buffer Idle', 'Requesting Microphone Permission', 'Initializing Audio', 'Microphone Buffer Active'];
    let status;
    let stream;
    let mediaStream;
    let audioContext;
    let sampleRate;
    let source;
    let scriptProcessor;
    let inputData;

    let options = {
      sampleRateCallback: undefined,
      bufferCallback: undefined,
      statusChangeCallback: undefined,
      bufferSize: 4096
    }

    if(arguments[0] && typeof arguments[0] === 'object') {
      for(let property in arguments[0]) {
        if(arguments[0].hasOwnProperty(property)) {
          options[property] = arguments[0][property];
        }
      }
    }

    const changeStatus = (newStatus) => {
      if(status !== newStatus) {
        status = newStatus;
        if(options.statusChangeCallback !== undefined) {
          options.statusChangeCallback(status, statusStrings[status]);
        }
      }
    }

    changeStatus(1);

    if(options.sampleRateCallback !== undefined) {
      if(!(options.sampleRateCallback && {}.toString.call(options.sampleRateCallback) === '[object Function]')) {
        changeStatus(0);
        throw new Error('sampleRateCallback is not a function.');
      }
    }

    if(options.bufferCallback !== undefined) {
      if(!(options.bufferCallback && {}.toString.call(options.bufferCallback) === '[object Function]')) {
        changeStatus(0);
        throw new Error('bufferCallback is not a function.');
      }
    }

    if(options.statusChangeCallback !== undefined) {
      if(!(options.statusChangeCallback && {}.toString.call(options.statusChangeCallback) === '[object Function]')) {
        changeStatus(0);
        throw new Error('statusChangeCallback is not a function.');
      }
    }

    if(!validBufferSizes.includes(options.bufferSize)) {
      changeStatus(0);
      throw new Error('bufferSize is not a valid buffer size. Supported values are 0, 256, 512, 1024, 2048, 4096, 8192, and 16384.');
    }

    if(!navigator.mediaDevices) {
      changeStatus(0);
      throw new Error('getUserMedia not supported in your browser.');
    }

    const start = () => {
      if(status <= 1) {
        if(navigator.mediaDevices) {
          setupUserInput();
        }
      }
    }

    const stop = () => {
      audioTeardown();
    }

    const setupUserInput = () => {
      changeStatus(2);
      navigator.mediaDevices.getUserMedia({audio: true, video: false})
      .then((stream) => {
        setupInputProccessing(stream);
      })
      .catch((error) => {
        audioTeardown();
        if(error.message === 'Permission denied') {
          changeStatus(1);
        } else {
          changeStatus(0);
          throw new Error(error);
        }
      });
    }

    const setupInputProccessing = (stream) => {
      changeStatus(3);
      mediaStream = stream;
      audioContext = new AudioContext();
      source = audioContext.createMediaStreamSource(mediaStream);
      scriptProcessor = audioContext.createScriptProcessor(options.bufferSize, 1, 1);

      source.connect(scriptProcessor);
      scriptProcessor.connect(audioContext.destination);

      if(options.sampleRateCallback !== undefined) {
        options.sampleRateCallback(audioContext.sampleRate);
      }

      if(options.bufferCallback !== undefined) {
        scriptProcessor.onaudioprocess = (event) => {
          options.bufferCallback(event.inputBuffer.getChannelData(0));
        }
      }

      changeStatus(4);
    }

    const audioTeardown = () => {
      if(scriptProcessor !== undefined) {
        scriptProcessor.disconnect();
      }
      if(source !== undefined) {
        source.disconnect();
      }
      if(audioContext !== undefined) {
        if(audioContext.state !== 'closed') {
          audioContext.close();
        }
      }
      if(mediaStream !== undefined) {
        mediaStream.getAudioTracks().forEach((track) => {track.stop()});
      }
      changeStatus(1);
    }

    return {
      start: start,
      stop: stop,
      status: status,
      statusStrings: statusStrings
    }
  };

  return MicBuffer;
})();