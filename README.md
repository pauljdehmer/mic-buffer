_**Mic-Buffer** — Javascript Library that wraps getting buffer data from a microphone._

## Usage

Include the library.

```html
<script src="mic-buffer.js"></script>
```

Create an instance of MicBuffer.

```js
const micBuffer = new MicBuffer({
  sampleRateCallback: handleSampleRate,
  bufferCallback: handleBuffer,
  statusChangeCallback: handleStatusChange,
  bufferSize: 256
});
```

Pass the constructor an object containing the following:

* sampleRateCallback - A function that receives a single parameter which is an integer representing the sample rate of the audio data in Hz. This gets fired after a user gives permission to access the microphone.

* bufferCallback - A function that receives a single parameter which is a Float32Array of the PCM data from the microphone. The length of the array is set by the bufferSize parameter.

* statusChangeCallback - A function that receives 2 parameters: 'status' which is an integer representing the status of the library, and 'statusString' which is a human readable version of the status. Status integers map to:

```bash
0 - 'Error'
1 - 'Microphone Buffer Idle'
2 - 'Requesting Microphone Permission'
3 - 'Initializing Audio'
4 - 'Microphone Buffer Active'
```

* bufferSize - An integer represenging the size of the buffer to fill before calling the bufferCallback. bufferSize must be a power of 2 between 256 and 16384, that is 256, 512, 1024, 2048, 4096, 8192 or 16384. Small numbers lower the latency, but large number may be necessary to avoid audio breakup and glitches. Default value is 4096.

None of these options are required.

The library also exposes the value of 'status' publicly.

```js
console.log(micBuffer.status)
```

See /example/index.html for a reference implementation of the library.